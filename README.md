# Lab 04 - Linked Structures -- Another Implementation of the BagInterface

See your CSC232 OneNote Class Notebook for details on what to do with this lab.

## Additional Notes

Each of the operations you are to implement have a paramter list that contains the following parameter:

`BagInterface<ItemType>* anotherBag`

In each of the operations, you'll want to access attributes of the bag, like `itemCount` and `headPtr`. In order to do this, you'll have to *cast* the parameter to the `LinkedBag` type. The following line shows you how to do this:

`LinkedBag<ItemType>* thatBag = dynamic_cast<LinkedBag<ItemType>*>(anotherBag);`

For more information on this type of casting, see [http://en.cppreference.com/w/cpp/language/dynamic_cast](http://en.cppreference.com/w/cpp/language/dynamic_cast)